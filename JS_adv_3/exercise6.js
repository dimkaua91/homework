const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};

const {name, surname, age = 50, salary = 'hidden'} = employee;

const employee2 = {
    name,
    surname,
    age,
    salary
};

console.log(employee2);
