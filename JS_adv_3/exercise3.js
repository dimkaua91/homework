const user1 = {
    name: "John",
    years: 30
};

const {name: name, years: age, isAdmin: isAdmin = false} = user1;

const user2 = {
    name,
    age,
    isAdmin
};

console.log(user2);
