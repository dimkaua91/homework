"use strickt"


const tabsList = document.querySelector('.tabs');
const tabsText = document.querySelectorAll('.tabs-content li');

tabsList.addEventListener('click', (event)=> {
    const target = event.target;
    const index = target.dataset.index;
   
    target.closest('ul').querySelector('.active').classList.remove('active');
    target.classList.add('active');
    
    tabsText.forEach((item) => {
        const tabsId = item.dataset.id;
        item.setAttribute('hidden', true);
        if (index === tabsId) {
            item.removeAttribute('hidden');
        }
    });

} );