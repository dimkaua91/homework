// Теоретический вопрос
// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

// try...catch - уместно использовать для сложного участка кода, чтобы скрипт продолжил свою работу дальше если возникнет какая-то ошибка и чтобы была возможность эту ошибку исправить. 


"use strict"

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

function bookList(array, parent) {
    const list = document.createElement('ul');
    document.querySelector(parent).append(list);

    array.forEach((item) => {
        try {
            if (!item.hasOwnProperty('author')) {
                throw new Error('нет свойства author');

            } else if (!item.hasOwnProperty('name')) {
                throw new Error('нет свойства name');

            } else if (!item.hasOwnProperty('price')) {
                throw new Error('нет свойства price');
                
            } else {
                const listItem = document.createElement('li');
                listItem.innerText = `${item.author} "${item.name}" - ${item.price}`;
                list.append(listItem);
            }
            
        } catch (elem) {
            console.error(elem);
        }
    });
}

bookList(books, '#root');
