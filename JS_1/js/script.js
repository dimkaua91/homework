// Теоретический вопрос

// 1)Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// 2)Почему объявлять переменную через var считается плохим тоном?

// 1. Разница в области видимости. var -может иметь как глобальную, так и локальную область видимости.
// let, const - имеют блочную видимость.
// 2.Значение var - устарело и можно встретить только в каких-то старых кодах, сейчас используют let и const.




const name = prompt ('What is your name?');
const age = prompt ('How old are you?');

if (age < 18) {
    alert ('You are not allowed to visit this website');

} else if (age >= 18 && age <= 22) {

    const confInform = confirm ('Are you sure you want to continue?');

    if (confInform) {
        alert (`Welcome, ${name}`);

    } else {
        alert ('You are not allowed to visit this website');
    }

} else {
    alert (`Welcome, ${name}`);
}