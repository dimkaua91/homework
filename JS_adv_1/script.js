// Теоретический вопрос
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

// Это возможность языка, которая помогает создавать новые обьекты на основе уже существующих методов.


class Employee {
    constructor (name,age,salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    get name () {
      return this._name;
    }
    get age () {
      return this._age
    }
  
    get salary () {
      return this._salary
    }
  
    set salary (value) {
      this._salary = value;
    }
  
    set name (value) {
      this._name = value;
    }
  
    set age (value) {
      this._age = value;
    }
  }
  
  class Programmer extends Employee {
    constructor (name,age,salary, lang ) {
      super(name,age,salary);
      this._lang = lang;
    }
  
    set salary (value) {
      this._salary = value;
    }
  
    get salary () {
      return this._salary * 3;
    }
  }

  
const programmer1 = new Programmer('Vadym', 19, 50000, 'en, de, ua');
const programmer2 = new Programmer('Petr', 20, 100000, 'en, de, ua');
const programmer3 = new Programmer('Yulia', 28, 60000, 'en, de, ua');
const programmer4 = new Programmer('Dima', 30, 70000, 'en, de, ua');

console.log(programmer1, programmer2, programmer3, programmer4);
