// Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.

// AJAX- это технология, которая позволяет взаимодействовать с сервером без перезагрузки страницы, он помогает избежать постоянной перезагрузки страницы в пределах одного веб-сервера.


class Films {
  constructor(characters, films, episodeId, filmTitle, openingCrawl) {
      this.films = films
      this.characters = characters
      this.episodeId = episodeId
      this.filmTitle = filmTitle
      this.openingCrawl = openingCrawl
      this.elements = {
          list: document.createElement('ul')
      }
  }

  renderFilmInfo(films, parent) {
      const {list} = this.elements
      parent.prepend(list)

      films.forEach((film) => {
          const listItem = document.createElement('li')
          listItem.id = `film_id_${film.id}`
          const episodeNum = document.createElement('p')
          const filmTitle = document.createElement('p')
          const filmCharacters = document.createElement('p')
          const openingCrawl = document.createElement('p')

          episodeNum.innerText = `Episode: ${film.episodeId}`
          filmTitle.innerText = `Name of film: ${film.name}`
          filmCharacters.innerText = 'The character: '
          openingCrawl.innerText = `Film content: ${film.openingCrawl}`

          filmCharacters.classList.add('character')

          listItem.append(episodeNum, filmTitle, filmCharacters, openingCrawl)
          list.append(listItem)

      })
  }

  static renderCharacters(character, filmID) {
      const filmBlock = document.querySelector(`#film_id_${filmID}`)
      if (!filmBlock) return;

      const filmCharacters = filmBlock.querySelector('.character')
      if (!filmCharacters) return;

      filmCharacters.innerText += ` ${character}, `
  }

  async getFilms() {
      this.films = await fetch('https://ajax.test-danit.com/api/swapi/films').then(r => r.json())
      this.renderFilmInfo(this.films, document.querySelector('body'))
      this.getCharacters(this.films)
  }

  async getCharacters(films) {
      await films.forEach(function (film) {
          const characters = film.characters

          characters.forEach(async function (link) {
              const character = await fetch(`${link}`)
                  .then(r => r.json())
                  .then(r => r.name)

              Films.renderCharacters(character, film.id)
          })
      })
  }
}

const film = new Films()
film.getFilms()

