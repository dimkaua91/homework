// Теоретический вопрос
// 1.Опишите своими словами, как Вы понимаете, что такое обработчик событий.

// 1) Обработчик события - по-сути это функция, которая срабатывает, тогда как событие произошло. Частые события это к примеру наведение мыши на какой-то объект, нажатие или отжание клавиши.



const input = document.createElement('input');
document.body.appendChild(input);
input.placeholder = 'Price';
input.type = 'number';


input.addEventListener('focus', function() {
    input.classList.add('focused');
    const element = document.querySelector('p');

    if (element) {
        element.remove();
        input.classList.remove('invalid');
    }
});

input.addEventListener('blur', function() {
    if (input.value === '') {
        input.classList.remove('focus');
    } else if (Math.sign(input.value) === -1) {
        input.classList.remove('focus');
        input.classList.add('invalid');
        
        const p = document.createElement('p');

        p.innerText = 'Please enter corect price';
        input.after(p);
    } else {
        input.classList.remove('focus');
        input.classList.add('valid');

        const span = document.createElement('span');
        span.innerText = `current Price ${input.value}`;

        const button = document.createElement('button');
        button.innerText = 'X';
        span.appendChild(button);
        input.before(span);
        button.addEventListener('click', () => removeElement(span));
    }
})

function removeElement(span) {
    span.remove();
    input.value = '';
}