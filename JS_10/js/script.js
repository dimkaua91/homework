"use strict"

const passwordForm = document.querySelector('.password-form');
const passwordIcons = document.querySelector('.fa-eye');
const firstPass = document.querySelector('.first-pass');
const secondPass = document.querySelector('.second-pass');
const errorText = document.createElement('pas');

passwordForm.addEventListener('click', function(event) {
    const input = event.target;
        if (input.getAttribute('type') === 'password') {
            input.setAttribute('type', 'text');
            passwordIcons.classList.add('hidden');
        } else {
            input.setAttribute('type', 'password');
            passwordIcons.classList.remove('hidden');
        }
    });

passwordForm.addEventListener('submit', function () {
    if (firstPass.value === secondPass.value && firstPass.value !== "") {
        if (document.querySelector('.errorText')) {
            document.querySelector('.errorText').remove();
        }
        alert('You are welcome!');

    } else {
        errorText.innerText = 'Нужно ввести одинаковые значения';
        errorText.classList.add('text');
        passwordForm.appendChild(errorText); 
    }
});
