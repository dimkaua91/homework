function changeTheme() {
    const touch = document.getElementById('theme_css').classList[0] === 'light';
    const element = document.getElementById('theme_css');
  
  
    if (touch) {
        element.href = '../style.css';
        element.classList.remove('light');
        element.classList.add('dark');
    } else {
        element.href = '../style-light.css';
        element.classList.remove('dark');
        element.classList.add('light');
    }
  };