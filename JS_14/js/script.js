$(document).on("click", ".hide-btn", function () {
    $('.top-rated').slideToggle(1000);
});

$(window).scroll(function () {
    if ($(this).scrollTop() >= $(this).innerHeight()) {
        $('.btn-up').fadeIn()
    } else {
        $('.btn-up').fadeOut()
    }
});

$('.btn-up').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 300)
});
