const btn = document.querySelector('.btn')
const ul = document.createElement('ul')

btn.addEventListener('click', async (event) => {
    event.target.classList.add('disabled')
    const request = await fetch('https://api.ipify.org/?format=json')
    const ip = await request.json()
    const phisicalAdress = await fetch(`http://ip-api.com/json/${ip.ip}?fields=status,continent,country,regionName,city,district&lang=ru`)
    const phisicalJ = await phisicalAdress.json()
    event.target.classList.remove('disabled')

    ul.innerHTML = `
<li>Континент - ${phisicalJ.continent}</li>
<li>Страна - ${phisicalJ.country}</li>
<li>Регион - ${phisicalJ.regionName}</li>
<li>Город - ${phisicalJ.city}</li>
<li>Район города - ${phisicalJ.district.length === 0 ? 'Error' : phisicalJ.district}</li>
`
    document.body.append(ul)
});
