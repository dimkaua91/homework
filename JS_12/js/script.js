// Теоретический вопрос
// 1.Опишите своими словами разницу между функциями setTimeout() и setInterval().
// 2.Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// 3.Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?


// 1)setTimeout вызывает функцию 1раз через определённый интервал времени, а setInterval вызывает функцию регулярно.
// 2)Сработает, сначала выполняться все синхронные действия, а после все, что передано в функцию  setTimeout.
// 3)Если не вызывать clearInterval, то setInterval будет вызывать функцию без останновки.






const stopTime = document.querySelector('#stop');
const startTime = document.querySelector('#resume');
const imageWrapper = document.querySelector('.images-wrapper');
const timer = document.getElementById('timer');
let currentImageIndex = 0;
const timeToNext = 3000;
let toNextImage = timeToNext;
let interval = createInterval()


startTime.addEventListener('click', () => {
    createInterval();
})

stopTime.addEventListener('click', () => {
    clearInterval(interval);
})

function createInterval() {
    return setInterval(() => {
        toNextImage -= 10
        if (toNextImage <= 0){
            toNextImage = timeToNext; 
            goToNextSlide()
        }
        timer.textContent = (toNextImage / 1000).toFixed(3);
    }); 
}

function goToNextSlide() {
    imageWrapper.children[currentImageIndex].hidden = true;
    currentImageIndex++
    if (currentImageIndex === imageWrapper.children.length) {
        currentImageIndex = 0;
    }

    imageWrapper.children[currentImageIndex].hidden = false;

}

goToNextSlide();